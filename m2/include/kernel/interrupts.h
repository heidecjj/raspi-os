// Taken from jsandler18's raspberry pi operating system tutorial
// https://jsandler18.github.io/
#include <kernel/peripheral.h>
#include <stdint.h>

#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#define INTERRUPTS_BASE (PERIPHERAL_BASE + INTERRUPTS_OFFSET)
#define INTERRUPTS_PENDING (INTERRUPTS_BASE + 0x200)

enum {
    GET_CHAR = 0, 
    PUT_CHAR,
    PRINT_STRING
};

void interrupts_init(void);

void do_swi(int r0, int r1, int r2, int r3);

#endif