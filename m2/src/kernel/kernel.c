#include "kernel/uart.h"
#include "kernel/stdio.h"
#include "kernel/interrupts.h"

int kernel_main(void) {
    uart_init();
    interrupts_init();

    char *hello = "Hello world\r\n";
    char *goodbye = "Goodbye world\r\n";
    do_swi(PRINT_STRING, hello, 0, 0);
    printf("Goodbye world\r\n");
    char recv;
    while (1) {
        recv = uart_getc();
        if (recv == 0x0D) uart_putc(0x0A);
        uart_putc(recv);
    }
}