// Taken from jsandler18's raspberry pi operating system tutorial
// https://jsandler18.github.io/
#include "kernel/interrupts.h"
#include "kernel/util.h"
#include "kernel/stdio.h"

extern void move_exception_vector(void);
extern uint32_t exception_vector;

void interrupts_init(void) {
    move_exception_vector();
}


void __attribute__ ((interrupt ("IRQ"))) irq_handler(void) {
    printf("IRQ HANDLER\r\n");
    while(1);
}

void __attribute__ ((interrupt ("ABORT"))) reset_handler(void) {
    printf("RESET HANDLER\rn");
    while(1);
}
void __attribute__ ((interrupt ("ABORT"))) prefetch_abort_handler(void) {
    printf("PREFETCH ABORT HANDLER\r\n");
    while(1);
}
void __attribute__ ((interrupt ("ABORT"))) data_abort_handler(void) {
    printf("DATA ABORT HANDLER\r\n");
    while(1);
}
void __attribute__ ((interrupt ("UNDEF"))) undefined_instruction_handler(void) {
    printf("UNDEFINED INSTRUCTION HANDLER\r\n");
    while(1);
}

void software_interrupt_handler(int r0, int r1, int r2, int r3) {
    switch (r0) {
        case PUT_CHAR:
            putc(r1);
            break;
        case GET_CHAR:
            * (char *) r1 = getc();
            break;
        case PRINT_STRING:
            printf((char *) r1);
            break;
    }
    return;
}

void __attribute__ ((interrupt ("FIQ"))) fast_irq_handler(void) {
    printf("FIQ HANDLER\r\n");
    while(1);
}