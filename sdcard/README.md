# SD Card Files
This directory contains the bare-minimum files required to run your own bare metal code on a Raspbery Pi 3. The files you need differs whether you are running 32-bit or 64-bit code. The kernels in this directory (`kernel7.img` and `kernel8.img`) are just dwelch67's bootloaders found [here](../bootloaders/).

## 32-bit
* bootcode.bin ([source](https://github.com/raspberrypi/firmware/blob/master/boot/bootcode.bin))
* start.elf ([source](https://github.com/raspberrypi/firmware/blob/master/boot/start.elf))
* kernel7.img ([bootloader-arm](../bootloaders/bootloader-arm/) or your own kernel)

Note: make sure you do not have `config.txt` on your SD card.

## 64-bit
* bootcode.bin ([source](https://github.com/raspberrypi/firmware/blob/master/boot/bootcode.bin))
* start.elf ([source](https://github.com/raspberrypi/firmware/blob/master/boot/start.elf))
* config.txt
* kernel8.img ([bootloader-aarch64](../bootloaders/bootloader-aarch64/) or your own kernel)