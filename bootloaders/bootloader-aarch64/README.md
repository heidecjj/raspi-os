# Serial Bootloader for 64-bit Raspberry Pi 3
Slightly modified version of dwelch67's [bootloader07 for 64-bit Raspberryp Pi 3](https://github.com/dwelch67/raspberrypi/tree/master/boards/pi3/aarch64/bootloader07). This bootloader will only work with 64-bit kernels.

When this bootloader is ready to receive a kernel you'll see some numbers and the string `IHEX`. This bootloader expects you to send it an Intel HEX formatted file using `minicom`. You can turn your kernel into an Intel HEX formatted file with the following:
```bash
arm-none-eabi-objcopy kernel.elf -O ihex kernel.hex
```
To send your hex file with `minicom`, you need to press `ctrl+a` then `s` in `minicom`, select `ascii` in the menu that pops up, and finally navigate to your kernel's hex file and select it. Once you send your hex file, type `g` to tell the bootloader to jump to the start of your code. For this bootloader, the address it jumps to is `0x80000`, so you need to make sure your kernel's linker file specifies `0x80000` as your start address. 