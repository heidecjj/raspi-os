# Serial Bootloaders for the Raspberry Pi 3

This directory contains two serial bootloaders for the Raspberry Pi 3 written by [dwelch67](https://github.com/dwelch67/raspberrypi/tree/master/boards/pi3/). See the READMEs in each directory for more information about each bootloader. `bootloader-aarch64/` is for 64-bit kernels while `bootloader-arm` is for 32-bit kernels.