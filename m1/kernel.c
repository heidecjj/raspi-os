#include "uart.h"

int kernel_main(void) {
    uart_init();
    uart_puts("Hello world");

    char recv;
    while (1) {
        recv = uart_getc();
        if (recv == 0x0D) uart_putc(0x0A);
        uart_putc(recv);
    }
}
