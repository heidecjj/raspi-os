// To keep this in the first portion of the binary.
.section ".text.boot"

// Make _start global.
.globl _start
_start:
    // disable 3 out of 4 cores
    mrc p15, #0, r1, c0, c0, #5
    and r1, r1, #3
    cmp r1, #0
    bne halt

    // seutp stack and call our main function
    mov sp,#0x8000
    bl kernel_main

halt: b halt

.globl PUT32
PUT32:
    str r1,[r0]
    bx lr

.globl GET32
GET32:
    ldr r0,[r0]
    bx lr

.globl dummy
dummy:
    bx lr

.globl GETPC
GETPC:
    mov r0,lr
    bx lr

.globl BRANCHTO
BRANCHTO:
    bx r0
