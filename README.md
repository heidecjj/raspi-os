# Raspberry Pi Operating Systems Development
This repo is a colleciton of resources I came across while trying to develop an operating system for the Raspberry Pi 3. This readme should hopefully help you get started developing bare metal code for the Raspberry Pi 3 as well as help you find examples and tutorials for implementing different operating systems concepts on the Raspberry Pi 3.

## Getting Started
### Setting up Your Development Environment
To start developing bare metal code for the Raspberry Pi 3, you'll need to install a cross compiler and you can optionally install qemu to emulate a Raspberry Pi for simple testing.

Run the following to install your cross compilers. The `arm-none-eabi` toolchain is used for 32-bit development on the Pi while the `aarch64-linux-gnu` toolchain is used for 64-bit development on the Pi.
```bash
# Ubuntu
sudo apt update
sudo apt install gcc-arm-none-eabi gcc-aarch64-linux-gnu
```
```bash
# Arch
sudo pacman -S yay
yay -S arm-none-eabi-gcc aarch64-linux-gnu-gcc
```

Run the following to install qemu.
```bash
# Ubuntu
# Your package manager probably doesn't have a version of qemu that can emulate the Raspberry Pi, so you should build it from source.
sudo apt update
sudo apt install build-essential zlib1g-dev pkg-config libglib2.0-dev binutils-dev libboost-all-dev autoconf libtool libssl-dev libpixman-1-dev libpython-dev python-pip python-capstone virtualenv xz-utils git wget
wget https://download.qemu.org/qemu-2.12.1.tar.xz
tar xvJf qemu-2.12.1.tar.xz
cd qemu-2.12.1
./configure --target-list=arm-softmmu,arm-linux-user,aarch64-softmmu,aarch64-linux-user
make -j 2
sudo make install
```
```bash
# Arch
sudo pacman -S glibc qemu qemu-arch-extra
```
`qemu-system-arm` is used for emulating 32-bit code built with the `arm-none-eabi` toolchain while `qemu-system-aarch64` is used for emulating 64-bit code build with the `aarch64-linux-gnu` toolchain.

### Running on an Actual Raspberry Pi
#### Hardware Required
To run your bare metal code on an actual Raspberry Pi (in our case the Raspberry Pi 3 Model B+) you'll need a few pieces of hardware:
* Raspberry Pi 3
* Micro SD Card w/ USB adapter or full size SD card adapter
* [USB-to-TTL Serial Cable](https://www.amazon.com/gp/product/B00DJUHGHI/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)

#### Minicom
You'll also need to install `minicom` to comminicate with the Pi over your USB-to-TTL cable. 
```bash
# Ubuntu
sudo apt install minicom
```
```bash
# Arch
sudo pacman -S minicom
```
`minicom` needs a bit of setup to work for the Pi. Run `sudo minicom -s` to open `minicom`'s configurator. Go to the `Serial port setup` tab, make sure `Serial Device` points to your USB-to-TTL cable (for me, this is `/dev/ttyUSB0`), `Bps/Par/Bits` should be set to `115200 8N1`, and `Hardware Flow Control` and `Software Flow Control` should both be set to `No`. If you want to, you can also turn on `Line Wrap` in the `Screen and keyboard` menu. Then `Save setup as dfl` and `Exit`. 

If you want to allow your non-root user to use `minicom`, first find the group that has permissions to read and write to `/dev/ttyS*` `/dev/ttyU*`:
```bash
ls -ls /dev/ttyS* /dev/ttyU*
```
This group might be `uucp` or `dialout`. Then just add your user (`your_username`) to that group:
```bash
sudo usermod -a -G dialout your_username
```

#### Setting up your SD Card
You don't need to have an SD card with raspbian in order to get this working, but I've been having trouble properly formatting my SD card. Using rasbian as a starting point for your SD card is much easier. Download [Raspbian Lite](https://downloads.raspberrypi.org/raspbian_lite_latest) and run the following to copy it onto your SD card.
```bash
# Where /dev/sdX is your sd card
unzip -p 2018-10-09-raspbian-stretch-lite.zip | sudo dd of=/dev/sdX bs=4M conv=fsync status=progress
```
You should now have 2 partitions on your SD card, `boot` and `rootfs`. `boot` is the partition we care about, `rootfs` doesn't really matter. Delete all the files in the `boot` partition and copy the files as specified in the [`sdcard/`](sdcard/) directory to the `boot` partition.

#### Running your kernel
Now that your SD card is set up, running your kernel on the Pi is pretty straight-forward. Follow the instructions in [`bootloaders/`](bootloaders/) if you're using one of the serial bootloaders, otherwise just copy your kernel to the `boot` partition of your SD card and rename it to `kernel7.img` if you compiled it using the `arm` toolchain or `kernel8.img` if you copiled it using the `aarch64` toolchain. Now eject your SD card and insert it into your Raspberry Pi.
Connect your USB-to-TTL cable to your Raspberry Pi and PC as shown below:
![](usb-to-ttl.png)

I recommend starting `minicom` before actually powering on your Raspberry Pi.

## Where to Find Good Examples of OS Topics
I started out developing a 32-bit kernel for the Raspberry Pi 3, but I think pursuing 64-bit development will be easier as I've found more useful 64-bit examples. These examples could be adapted to 32-bit arm with modifications to any assembly files used and a change in toolchain. One thing to keep in mind when using these examples is that you'll probably have to modify their Makefiles to use the toolchain you have installed (`arm-none-eabi` or `aarch64-linux-gnu`) and you might have to modify start addresses in linker scripts if you're using the serial bootloaders in [`bootloaders/`](bootloaders/).

I reccomend staring from an OS tutorial ([jsandler](https://jsandler18.github.io/) for 32-bit or [s-matyukevich](https://github.com/s-matyukevich/raspberry-pi-os) for 64-bit) and then adding in aditional features you care about. Usually topics like multi-tasking and memory management appear near the end of these OS turorials when each author is building upon a medium-sized code base, so you'll have to do a lot of work refactoring their code to work with your own code. It would be easier to follow an OS tutorial and add in the features that tutorial is missing.

### Serial Communication
Serial communication isn't too hard to get working and is usually the first thing people show in their Raspberry Pi OS or Raspberry Pi bare metal tutorials. My [`m1/`](m1/) directory contains a simple "Hello World" 32-bit kernel built upon dwelch67's [`uart05`](https://github.com/dwelch67/raspberrypi/tree/master/uart05) project.
s-matyukevich has a good explanation of UART communication on the Raspberry Pi 3 in his [`lesson01`](https://github.com/s-matyukevich/raspberry-pi-os/blob/master/docs/lesson01/rpi-os.md).
bztsrc has 64-bit examples of UART communication for both the Pi's Mini UART interface ([`uart1`](https://github.com/bztsrc/raspi3-tutorial/tree/master/03_uart1)) and full UART interface ([`uart0`](https://github.com/bztsrc/raspi3-tutorial/tree/master/05_uart0)).

### Interrupts
Both OS tutorials include sections on interrupts, see dwelch67's [part07](https://jsandler18.github.io/tutorial/interrupts.html) and s-matyukevich's [lesson03](https://github.com/s-matyukevich/raspberry-pi-os/blob/master/docs/lesson03/rpi-os.md). Both of these tutorials do not cover software interrupts, although software interrupts are mentioned in both. There's also a big difference in exception vectors between 32-bit and 64-bit kernels, so these tutorials only work for one architecture. These types of interrutps are most important for setting up timers needed for handling multiple processes later.

My [`m2/`](m2/) directory has an example of software interrupts for 32-bit kernels using the `svc` instruction. My example works in qemu, but I haven't been able to get it working on actual hardware. My example also doesn't handle switching execution levels. s-matyukevich has a good 64-bit tutorial in his [lesson05](https://github.com/s-matyukevich/raspberry-pi-os/blob/master/docs/lesson05/rpi-os.md) for implementing system calls and spawning user processes at a different execution level than the kernel.

### Reading and Writing to an SD Card
bztsrc has working examples of how to read sectors from an SD card, [`0B_readsector`](https://github.com/bztsrc/raspi3-tutorial/tree/master/0B_readsector). This example uses the 64-bit toolchain. I have not been able to find working 32-bit examples for reading from the Pi's SD card. bztsrc doesn't provide an example of writing to the SD card, but provides some hints in a few issues, see [issue#18](https://github.com/bztsrc/raspi3-tutorial/issues/18) and [issue#46](https://github.com/bztsrc/raspi3-tutorial/issues/46). You might want to reach out to [OllieLollie1](https://github.com/OllieLollie1/Raspi3-Kernel) as he says in issue#46 that he might have code that works for writing to the SD card.

### Memory Management
jsandler18 has two tutorials for memory management, [`part04`](https://jsandler18.github.io/tutorial/wrangling-mem.html) and [`part05`](https://jsandler18.github.io/tutorial/dyn-mem.html). He creates a simple paging system for memory and implements malloc and free for his memory system. jsandler18's memory management code was written for a 32-bit kernel but should work with a 64-bit kernel.

bztsrc and s-matyukevich both provide tutorials for using the ARM core's memory management unit to implement virtual memory, see [10_virtualmemory](https://github.com/bztsrc/raspi3-tutorial/tree/master/10_virtualmemory) and [lesson06](https://github.com/s-matyukevich/raspberry-pi-os/blob/master/docs/lesson06/rpi-os.md). I have not experimented virtual memory.

### Processes
Both OS tutorials mentioned earlier have lessons for handling processes, see jsandler's [part08](https://jsandler18.github.io/tutorial/process.html) and s-matyukevich's [lesson04](https://github.com/s-matyukevich/raspberry-pi-os/blob/master/docs/lesson04/rpi-os.md). I have not expirimented with either author's implementation. jsandler's tutorial is simpler than s-matyukevich's more detailed tutorial which even includes sytem calls and user processes as mentioned above in his [lesson05](https://github.com/s-matyukevich/raspberry-pi-os/blob/master/docs/lesson05/rpi-os.md).

### Using a Real Screen
Both jsandler and bztsrc have tutorials for displaying to a real monitor, see jsandler's [part06](https://jsandler18.github.io/tutorial/hdmi.html) and bztsrc's [09_framebuffer](https://github.com/bztsrc/raspi3-tutorial/tree/master/09_framebuffer) and [0A_pcscreenfont](https://github.com/bztsrc/raspi3-tutorial/tree/master/0A_pcscreenfont). These examples use the Pi's HDMI port to display to a monitor. I have not expirimented with either author's examples, but I've come to trust the code bztsrc produces. 

## Repositories to be aware of
### [Bare Metal Programming on Raspberry Pi 3 - bztsrc](https://github.com/bztsrc/raspi3-tutorial)
This repo contains many standalone 64-bit bare metal applications for the Pi 3. This is not an operating systems tutorial. It contains applications dealing with the following:
uart1, mailboxes, uart0, random, delays, power, display, fonts, reading sectors from SD, reading directories, reading files, initial ramdisks, execution levels, virtual memory, exceptions, printf, interactive debugger within exception handler, and an application that can boot kernels over a serial connection. This guy wrote the code to include the Pi 3 in qemu, so he knows what he’s doing. He is also quick to responding to issues on his github.

### [Building an Operating System for the Raspberry Pi - jsandler18](https://jsandler18.github.io/)

This is a static site hosted on github containing an operating systems tutorial for 32-bit code on the original Pi and the Pi 2. Pi 2 code seems to work on the Pi 3 B+. This tutorial goes over getting something to boot, memory management, display, interrupts (for things like timers, not software interrupts), processes, and locks. The author includes his code in each lesson. Each lesson has a link to a different commit within the same repository. Cloning the repository will not give you all the lessons directly, just the last lesson.

### [Learning operating system development using Linux kernel and Raspberry Pi - s-matyukevich](https://github.com/s-matyukevich/raspberry-pi-os)
This repo contains a tutorial for creating a 64-bit operating system for the Raspberry Pi 3. The author explains operating systems concepts very well and compares the operating system his tutorial makes to the linux kernel. This tutorial includes topics including kernel initialization, processor initialization, interrupt handling, process scheduling, user processes and system calls, and virtual memory management.

### [Raspberry Pi ARM based bare metal examples - dwelch67](https://github.com/dwelch67/raspberrypi)
This repo has many 32-bit bare metal applications for the original Pi. The documentation can be hard to read but the author provides a good serial bootloader that I used extensively and included in this repository in [`bootloaders/`](bootloaders/)

### [BareMetal Raspberry-Pi (Linux free zone) - LdB-ECM](https://github.com/LdB-ECM/Raspberry-Pi)
Not super useful for developing our operating system, but this repo has bare metal code for a Usb driver, multicore execution, and graphics all for the Pi 1 through Pi 3.

### [Bare metal boot loader for the Raspberry Pi's VideoCore processor (no ARM!) - davidgiven](https://github.com/davidgiven/piface)
Interesting project that only runs on the Pi’s GPU. Acts as a bootloader with support for uploads and downloads via XMODEM, read/write support for SD cards, code execution, and modifying and dumping memory. I have not been able to get it to work though.

### [Circle - Bare Metal C++ Development - rsta2](https://github.com/rsta2/circle)
This project aims to provide an easy way to write bare metal C++ code that utilizes the hardware on a raspberry pi. This project works with all raspberry pi models.

### [Raspberry Pi Bare Metal Assembly Programming - PeterLemon](https://github.com/PeterLemon/RaspberryPi)
This repo has various bare metal assembly programs for the original Pi, Pi 2, and Pi 3. I didn’t end up using this repo and the documentation for what each program does is a bit lacking.

### [Raspberry Pi Bare Metal Light Shows - vanvught](https://github.com/vanvught/rpidmx512)
I haven’t looked deeply into this repo, I just stumbled upon it when I was looking for SD card reading examples.
